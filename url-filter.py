#!/usr/bin/env python3

import subprocess
import sys
from urllib.parse import urlparse

import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify

chromium = {
    'accounts.google.com',
    'docs.google.com',
    'drive.google.com',
    'hangouts.google.com',
    'issuetracker.google.com',
    'summerofcode.withgoogle.com',
    'plus.google.com',
}

chromium_incognito = {
    'meet.jit.si',
    'localhost',
#    '',  # file:/// URIs
}

ydl = {
    'www.youtube.com',
    'youtube.com',
    'youtu.be',
    'vimeo.com',
}

with open('/tmp/url-filter.log', 'a') as fp:
    fp.write('---------------------------------------\n%s\n' % ' '.join(sys.argv))

    for arg in sys.argv[1:]:
        url = urlparse(arg.replace('http://', 'https://'))
        cmd = []
        if url.netloc in chromium:
            cmd = ['chromium-browser', arg]
        elif url.netloc in chromium_incognito:
            cmd = ['chromium-browser', '--incognito', arg]
        elif url.netloc in ydl:
            cmd = ['ydl', arg]
            Notify.init ('Downloading...')
            n = Notify.Notification.new ('ydl',
                                         'Downloading ' + arg,
                                         'dialog-information')
            n.show()
        else:
            cmd = ['firefox', arg]
        subprocess.Popen(cmd)
        fp.write('\t%s\n' % ' '.join(cmd))
