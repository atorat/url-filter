
# url-filter

URL filters and redirectors for GNU/Linux inspired by Android's Intent Filters.

## Installation

```console
$ mkdir ~/.local/share/applications/
$ cp url-filter.desktop ~/.local/share/applications/
$ sudo update-desktop-database
$ xdg-settings set default-web-browser url-filter.desktop
$ xdg-open https://meet.jit.si/TestingTesting123
$ sudo update-alternatives --install /usr/bin/x-www-browser x-www-browser /path/to/url-filter.py 100
$ sudo update-alternatives --install /usr/bin/gnome-www-browser gnome-www-browser /path/to/url-filter.py 100
```
For more info, see:
https://askubuntu.com/questions/1161752/how-can-i-configure-a-domain-specific-default-browser
